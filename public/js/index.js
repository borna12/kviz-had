  // VARIABLE DECLARATIONS ------

  // pages
  var initPage,
      questionsPage,
      resultsPage,
      // buttons
      startBtn,
      submitBtn,
      continueBtn,
      retakeBtn,
      spanishBtn,
      // question and answers
      question,
      answerList,
      answerSpan,
      answerA,
      answerB,
      answerC,
      answerD,
      // event listeners
      answerDiv,
      answerDivA,
      answerDivB,
      answerDivC,
      answerDivD,
      feedbackDiv,
      selectionDiv,
      toBeHighlighted,
      toBeMarked,
      userScore,
      // quiz
      quiz,
      questionCounter,
      correctAnswer,
      correctAnswersCounter,
      userSelectedAnswer,
      // function names
      newQuiz,
      generateQuestionAndAnswers,
      getCorrectAnswer,
      getUserAnswer,
      selectAnswer,
      deselectAnswer,
      selectCorrectAnswer,
      deselectCorrectAnswer,
      getSelectedAnswerDivs,
      highlightCorrectAnswerGreen,
      highlightIncorrectAnswerRed,
      slikica,
      clearHighlightsAndFeedback;


  $(document).ready(function() {

      // DOM SELECTION ------

      // App pages
      // Page 1 - Initial
      initPage = $('.init-page');
      // Page 2 - Questions/answers
      questionsPage = $('.questions-page');
      // Page 3 - Results
      resultsPage = $('.results-page');
      slikica = $('.slikica');

      // Buttons
      startBtn = $('.init-page__btn, .results-page__retake-btn');
      submitBtn = $('.questions-page__submit-btn');
      continueBtn = $('.questions-page__continue-btn');
      retakeBtn = $('.results-page__retake-btn');
      spanishBtn = $('.results-page__spanish-btn');

      // Answer block divs
      answerDiv = $('.questions-page__answer-div');
      answerDivA = $('.questions-page__answer-div-a');
      answerDivB = $('.questions-page__answer-div-b');
      answerDivC = $('.questions-page__answer-div-c');
      answerDivD = $('.questions-page__answer-div-d');

      // Selection div (for the pointer, on the left)
      selectionDiv = $('.questions-page__selection-div');

      // Feedback div (for the checkmark or X, on the right)
      feedbackDiv = $('.questions-page__feedback-div');

      // Questions and answers
      question = $('.questions-page__question');
      answerList = $('.questions-page__answer-list');
      answerSpan = $('.questions-page__answer-span');
      answerA = $('.questions-page__answer-A');
      answerB = $('.questions-page__answer-B');
      answerC = $('.questions-page__answer-C');
      answerD = $('.questions-page__answer-D');


      // User final score
      userScore = $('.results-page__score');

      // QUIZ CONTENT ------
      quiz = [{
          question: "Hrvatski državni arhiv središnja je arhivska ustanova nadležna za prikupljanje i obradu arhivskoga gradiva značajnog za Republiku Hrvatsku?",
          answers: ["Točno", "Netočno", "", ""],
          correctAnswer: "Točno",
          slika: "slike/1.jpg",
          slika2: "slike/2.jpg",
          komentar: "Hrvatski državni arhiv, središnja i matična arhivska ustanova, čuva, štiti, stručno obrađuje te daje na korištenje arhivsko gradivo. Osnovna je zadaća Hrvatskoga državnog arhiva prikupiti i sačuvati najvrjednije arhivsko gradivo i učiniti ga trajno dostupnim svakomu građaninu. U Hrvatskom državnom arhivu čuva se više od 29 000 dužnih metara gradiva raspoređeno u 2150 arhivskih fondova i zbirki nastalih djelovanjem središnjih tijela državne uprave i pravosuđa, prosvjetnih, kulturnih, zdravstvenih i vojnih ustanova, kao i djelatnošću istaknutih pojedinaca i obitelji te hrvatskoga iseljeništva.",
          opis: "<em>Hrvatski državni arhiv</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Kojim je umjetničkim stilom s početka 20.st. izgrađena zgrada Hrvatskoga državnoga arhiva?",
          answers: ["Klasicizam", "Secesija", "Funkcionalizam", "Ekspresionizam"],
          correctAnswer: "Secesija",
          slika: "slike/secesija.jpg",
          slika2: "slike/luster.jpg",
          komentar: "Riječ je o jednoj od najljepših secesijskih zgrada na širemu geografskom prostoru i jedinstvenome primjerku <em>gesamtkunstwerka</em> u Hrvatskoj. Visoka polukupola nad Velikom čitaonicom u središnjemu dijelu palače s četiri stupa, na kojima se nalaze četiri sove koje nose globuse, savršena je vizualna alegorija znanja.",
          opis: "<em>Luster u Velikoj čitaonici Hrvatskoga državnog arhiva</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Tko je autor freske prikazane na slici?",
          answers: ["Vlaho Bukovac", "Mirko Rački", "Ivan Tišov", "Robert Auer"],
          correctAnswer: "Vlaho Bukovac",
          slika: "slike/bukovac.jpg",
          slika2: "slike/slikari.jpg",
          komentar: "Na unutarnjemu uređenju reprezentativnih prostora sudjelovali su ponajbolji hrvatski slikari i kipari. Južni zid Velike čitaonice ukrašava veliko platno Razvitak hrvatske kulture Vlahe Bukovca, a iznad glavnoga izlaza te lijevo i desno od njega nalaze se platna Mirka Račkoga Znanost u Starome vijeku, Znanost u Srednjemu vijeku te Znanost u Novome vijeku. Također valja istaknuti platna Ivana Tišova i Roberta Auera koja se nalaze u Profesorskoj čitaonici.",
          opis: "<em>Velika čitaonica</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Što je prikazano na slici?",
          answers: ["Škrinja sjećanja", "Škrinja dokumenata", "Škrinja privilegija", "Škrinja banova"],
          correctAnswer: "Škrinja privilegija",
          slika: "slike/skrinja11.jpg",
          slika2: "slike/skrinja.jpg",
          komentar: "Sve do 17. stoljeća  u Hrvatskoj se, kao i svuda u Europi, arhivsko gradivo čuvalo po crkvama i samostanima gdje su se pohranjivale važne listine i oporuke. Blagajnik kraljevstva te kasnije zemaljski protonotar, magistar Ivan Zakmardi Dijankovečki, temeljem odluke Hrvatskoga sabora dao je izraditi hrastovu škrinju u kojoj su se čuvale temeljne isprave hrvatske države. Bilo je to 23. prosinca 1643. Taj se datum simbolički uzima kao osnutak državnoga arhiva.",
          opis: "<em>Škrinja je imala zaštitnu bravu koja se otključavala s tri različita ključa. Jedan je čuvao ban, drugi podban, a treći protonotar. Tim su se redom ključevi stavljali u bravu i tek se tada škrinja  mogla otvoriti.</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Što se čuva u javnim arhivima u RH?",
          answers: ["arhivsko gradivo državnih tijela i ukupne javne uprave", "ukupno javno arhivsko gradivo", "javno i privatno arhivsko gradivo od značaja za društvenu zajednicu", "registraturno arhivsko gradivo"],
          correctAnswer: "javno i privatno arhivsko gradivo od značaja za društvenu zajednicu",
          slika: "slike/1.jpg",
          slika2: "slike/vparun2.jpg",
          komentar: "U arhivima u Hrvatskoj čuva se i javno i privatno arhivsko gradivo temeljem čl. 2. st. 2. Zakona o arhivskome gradivu i arhivima. O zaštiti privatnoga gradiva govori cijelo treće poglavlje toga Zakona.",
          opis: "<em>„Dvostruki“ rukopis Vesne Parun: stara pjesma pisana četrdesetih godina 20. st., te nadopisan komentar autorice s kraja 80-ih</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Koliko se fotografija čuva u Hrvatskom državnom arhivu?",
          answers: ["manje od 750 000", "više od 5 000 000", "više od 1 750 000", "između 750 000 i 1 000 000"],
          correctAnswer: "više od 1 750 000",
          slika: "slike/fotografija.jpg",
          slika2: "slike/mikrofilm.jpg",
          komentar: "Hrvatski državni arhiv čuva više od 1 750 000 fotografija te preko deset milijuna mikrofilmskih snimaka dokumenata iz različitih ustanova Hrvatske i inozemstva. Također čuva i preko 20 000 listova povijesnih karata.",
          opis: "<em>Mikrofilm je analogni medij u obliku fotografskoga neperforiranoga filma (tzv. master negativ) širine 35 mm, na koji se najprije dokumenti snime, a potom se kopiranjem izrade sigurnosne i zaštitne kopije (jedan negativ i nekoliko pozitiva).</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Koji, za hrvatsku povijest izrazito važan dokument se nalazi na slici, a čuva se u Hrvatskom državnom arhivu?",
          answers: ["Riječka krpica (Hrvatsko-ugarska nagodba)", "Trogirska diploma", "Povelja kneza Trpimira", "Vinodolski zakonik"],
          correctAnswer: "Riječka krpica (Hrvatsko-ugarska nagodba)",
          slika: "slike/krpica.jpg",
          slika2: "slike/krpica.jpg",
          komentar: "Riječka krpica 66. je članak Hrvatsko-ugarske nagodbe, i predstavlja završetak višegodišnjih prijepora u pregovorima između Hrvatske i Ugarske, kojoj je Rijeka trebala poslužiti kao izlaz na more i važna trgovinsko-prometna luka. Kako pregovori nisu uspjeli, dogovoreno je da Rijeka ostane <em>corpus separatum</em>, odnosno grad s poluautonomnim statusom. Ipak, Mađari su u dokument koji je vladar Franjo Josip II. već potpisao, nalijepili novi čl. 66. u kojem stoji da su <em>corpus separatum<em> grad, ali i luka te kotar Rijeka, što im je znatno olakšalo korištenje luke u vlastite interese.",
          opis: "<em>Hrvatsko-ugarska nagodba s <em>Riječkom krpicom<em> čuva se u Hrvatskome državnom arhivu, arhivski fond HR-HDA-65. Sabor Kraljevina Hrvatske, Slavonije i Dalmacije (1861-1918).</em>",
          boja_pozadine: "#e0e0e0"         
      }, {
          question: "Po čemu je značajan film <em>Lisinski</em> iz 1944. godine?",
          answers: ["Riječ je o prvom hrvatskom zvučnom filmu", "Riječ je o prvom hrvatskom filmu u boji", "Riječ je o prvom hrvatskom dugometražnom filmu", "Riječ je o prvom hrvatskom crtanom filmu"],
          correctAnswer: "Riječ je o prvom hrvatskom dugometražnom filmu",
          slika: "slike/lisinski.jpg",
          slika2: "slike/sesir.jpg",
          komentar: "Hrvatska kinoteka kao nacionalni filmski arhiv djeluje od 1979. godine i to kao poseban odjel pri Hrvatskom državnom arhivu. Temeljna joj je zadaća prikupljanje, čuvanje, obrada, zaštita i korištenje filmskog gradiva proizvedenog na području Republike Hrvatske. Vrijedna nacionalna filmska baština obuhvaća 6000 naslova audiovizualnih djela od 1904. do danas te popratno filmsko gradivo.",
          opis: "<em>Film <em>Šešir</em> Oktavijana Miletića iz 1937. godine smatra se prvim hrvatskim zvučnim filmom.</em>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Najstariji dokument koji se čuva u Hrvatskom državnom arhivu jest <em>Oporuka Agapite</em>. Znate li koje je godine nastao ovaj dokument?",
          answers: ["499.", "999.", "1499.", "1299."],
          correctAnswer: "999.",
          slika: "slike/oporuka1.jpg",
          slika2: "slike/Grbovnica_Kraljevine_Slavonije_HDA_1009.jpg",
          komentar: "Najstarije gradivo čine manje cjeline i pojedinačni dokumenti sabrani u nekoliko dragocjenih arhivskih zbirki: Zbirka najstarijih hrvatskih povelja, Zbirka srednjovjekovnih isprava i Hrvatske plemićke obitelji i vlastelinstva. Arhiv čuva i brojne glagoljske rukopise i isprave u Zbirci glagoljskih rukopisa, Zbirci glagoljskih isprava, fondovima pavlinskih samostana, isusovačkoga samostana u Rijeci te hrvatskoglagoljskoga notarijata otoka Krka.",
          opis: "<strong>Grbovnica Kraljevine Slavonije iz zbirke Hrvatskog državnog arhiva</strong>",
          boja_pozadine: "#e0e0e0"
      }, {
          question: "Osim Hrvatskog državnog arhiva, koliko još u Hrvatskoj postoji javnih arhivskih ustanova?",
          answers: ["19", "12", "26", "35"],
          correctAnswer: "19",
          slika: "slike/dubrovnik.jpg",
          slika2: "slike/drzavniarhivi.jpg",
          komentar: "Uz Hrvatski državni arhiv u Republici Hrvatskoj postoji mreža područnih državnih arhiva. Oni obavljaju arhivsku službu u odnosu na arhivsko i registraturno gradivo državnih tijela, pravnih osoba s javnim ovlastima i javnih službi koje djeluju na njihovom području. U njima se nalaze fondovi regionalnih upravnih i sudskih tijela, lokalnih škola, bolnica, tvornica, kulturnih udruženja, zbirke posvećene temama od lokalnoga i širega značenja, obiteljski i osobni fondovi te razno drugo gradivo koje predstavlja vrijedan izvor za proučavanje političke, gospodarske i kulturne povijesti gradova i šire regije. To su Državni arhivi u: Bjelovaru, Dubrovniku, Gospiću, Karlovcu, Križevcima, Osijeku, Pazinu, Rijeci, Sisku, Slavonskome Brodu, Splitu, Šibeniku, Varaždinu, Virovitici, Vukovaru, Zadru, Zagrebu, te Državni arhiv za Međimurje.",
          opis: "<em>Na mrežnim stranicama Hrvatskog arhivističkog društva možete pronaći interaktivnu kartu s popisom svih područnih državnih arhiva. <a href='https://had-info.hr/arhivi-u-hrvatskoj' target='_blank'>Pritisnite ovdje za pregled karte.</a></em>",
          boja_pozadine: "#e0e0e0"
      }];


      function shuffle(array) { //izmješaj pitanja
          var i = 0,
              j = 0,
              temp = null

          for (i = array.length - 1; i > 0; i -= 1) {
              j = Math.floor(Math.random() * (i + 1))
              temp = array[i]
              array[i] = array[j]
              array[j] = temp
          }
      }



      // FUNCTION DECLARATIONS ------
      $.fn.declasse = function(re) {
              return this.each(function() {
                  var c = this.classList
                  for (var i = c.length - 1; i >= 0; i--) {
                      var classe = "" + c[i]
                      if (classe.match(re)) c.remove(classe)
                  }
              })
          }
          // Start the quiz
      newQuiz = function() {

          // Set the question counter to 0
          questionCounter = 0;

          // Set the total correct answers counter to 0
          correctAnswersCounter = 0;

          // Hide other pages of the app
          questionsPage.hide();
          resultsPage.hide();

      };

      // Load the next question and set of answers
      generateQuestionAndAnswers = function() {
          question.html("<span style='font-size: 1.3rem;'>" + (questionCounter + 1) + "/" + quiz.length + ".</span> <br>" + quiz[questionCounter].question);
          shuffle(quiz[questionCounter].answers);
          answerA.text(quiz[questionCounter].answers[0]);
          if (answerA.html() == "" || null) {
              answerDivA.hide()
          } else {
              answerDivA.show()
          };
          answerB.text(quiz[questionCounter].answers[1]);
          if (answerB.html() == "" || null) {
              answerDivB.hide()
          } else {
              answerDivB.show()
          };
          answerC.text(quiz[questionCounter].answers[2]);
          if (answerC.html() == "" || null) {
              answerDivC.hide()
          } else {
              answerDivC.show()
          };
          answerD.text(quiz[questionCounter].answers[3]);
          if (answerD.html() == "" || null) {
              answerDivD.hide()
          } else {
              answerDivD.show()
          };


          slikica.attr("src", quiz[questionCounter].slika)
          slikica.attr("data-zoom-image", quiz[questionCounter].slika)
          $("#opis").html("<em>" + quiz[questionCounter].opis + "</em>")



      };

      // Store the correct answer of a given question
      getCorrectAnswer = function() {
          correctAnswer = quiz[questionCounter].correctAnswer;
      };

      // Store the user's selected (clicked) answer
      getUserAnswer = function(target) {
          userSelectedAnswer = $(target).find(answerSpan).text();
      };

      // Add the pointer to the clicked answer
      selectAnswer = function(target) {
          $(target).find(selectionDiv).addClass('ion-chevron-right');
          $(target).addClass("odabir")
      };

      // Remove the pointer from any answer that has it
      deselectAnswer = function() {
          if (selectionDiv.hasClass('ion-chevron-right')) {
              selectionDiv.removeClass('ion-chevron-right');
              selectionDiv.parent().removeClass("odabir")
          }
      };

      // Get the selected answer's div for highlighting purposes
      getSelectedAnswerDivs = function(target) {
          toBeHighlighted = $(target);
          toBeMarked = $(target).find(feedbackDiv);
      };

      // Make the correct answer green and add checkmark
      highlightCorrectAnswerGreen = function(target) {
          if (correctAnswer === answerA.text()) {
              answerDivA.addClass('questions-page--correct');
              answerDivA.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerB.text()) {
              answerDivB.addClass('questions-page--correct');
              answerDivB.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerC.text()) {
              answerDivC.addClass('questions-page--correct');
              answerDivC.find(feedbackDiv).addClass('ion-checkmark-round');
          }
          if (correctAnswer === answerD.text()) {
              answerDivD.addClass('questions-page--correct');
              answerDivD.find(feedbackDiv).addClass('ion-checkmark-round');
          }
      };

      // Make the incorrect answer red and add X
      highlightIncorrectAnswerRed = function() {
          toBeHighlighted.addClass('questions-page--incorrect');
          toBeMarked.addClass('ion-close-round');
      };

      // Clear all highlighting and feedback
      clearHighlightsAndFeedback = function() {
          answerDiv.removeClass('questions-page--correct');
          answerDiv.removeClass('questions-page--incorrect');
          feedbackDiv.removeClass('ion-checkmark-round');
          feedbackDiv.removeClass('ion-close-round');
      };

      // APP FUNCTIONALITY ------

      /* --- PAGE 1/3 --- */

      // Start the quiz:
      newQuiz();

      // Clicking on start button:
      startBtn.on('click', function() {

          // Advance to questions page
          initPage.hide();
          questionsPage.show(300);

          // Load question and answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();

      });

      /* --- PAGE 2/3 --- */

      // Clicking on an answer:
      answerDiv.on('click', function() {

          // Make the submit button visible
          submitBtn.show(300);

          // Remove pointer from any answer that already has it
          deselectAnswer();

          // Put pointer on clicked answer
          selectAnswer(this);

          // Store current selection as user answer
          getUserAnswer(this);

          // Store current answer div for highlighting purposes
          getSelectedAnswerDivs(this);

      });

      // Clicking on the submit button:
      submitBtn.on('click', function() {

          // Disable ability to select an answer
          answerDiv.off('click');

          // Make correct answer green and add a checkmark
          highlightCorrectAnswerGreen();

          // Evaluate if the user got the answer right or wrong
          if (userSelectedAnswer === correctAnswer) {

              // Increment the total correct answers counter
              correctAnswersCounter++;
              swal({
                  title: "Točno",
                  html: "<br>" + quiz[questionCounter].komentar + "<br><br><em>Slika: " + quiz[questionCounter].opis + "</em><br><br><img src='" + quiz[questionCounter].slika2 + "'class='slikica2'/>",
                  showCloseButton: true,
                  confirmButtonText: ' dalje',
                  backdrop: false

              });
              $('.slikica2').lightzoom({
                  glassSize: 175,
                  zoomPower: 2
              });
          } else {
              highlightIncorrectAnswerRed();
              swal({
                  title: "Netočno",
                  html: "Točan odgovor je: " + quiz[questionCounter].correctAnswer + "<br><br>" + quiz[questionCounter].komentar + "<br><br><em>Slika: " + quiz[questionCounter].opis + "</em><br><br><img src='" + quiz[questionCounter].slika2 + " 'class='slikica2'/>",
                  showCloseButton: true,
                  confirmButtonText: ' dalje',
                  backdrop: false
              });
              $('.slikica2').lightzoom({
                  glassSize: 175,
                  zoomPower: 2
              });
          }

          // Substitute the submit button for the continue button:
          submitBtn.hide(300);
          continueBtn.show(300);

      });

      // Clicking on the continue button:
      continueBtn.on('click', function() {

          // Increment question number until there are no more questions, then advance to the next page
          if (questionCounter < quiz.length - 1) {
              questionCounter++;
          } else {
              questionsPage.hide();
              resultsPage.show(300);
              // Display user score as a percentage
              userScore.text(Math.floor((correctAnswersCounter / quiz.length) * 100) + "%");
          }

          // Load the next question and set of answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Remove all selections, highlighting, and feedback
          deselectAnswer();
          clearHighlightsAndFeedback();

          // Hide the continue button
          continueBtn.hide(300);

          // Enable ability to select an answer
          answerDiv.on('click', function() {
              // Make the submit button visible
              submitBtn.show(300);
              // Remove pointer from any answer that already has it
              deselectAnswer();
              // Put pointer on clicked answer
              selectAnswer(this);
              // Store current answer div for highlighting purposes
              getSelectedAnswerDivs(this);
              // Store current selection as user answer
              getUserAnswer(this);
          });

      });

      /* --- PAGE 3/3 --- */

      // Clicking on the retake button:
      retakeBtn.on('click', function() {

          // Go to the first page


          // Start the quiz over
          newQuiz();
          resultsPage.hide();
          questionsPage.show(300);

          // Load question and answers
          generateQuestionAndAnswers();

          // Store the correct answer in a variable
          getCorrectAnswer();

          // Hide the submit and continue buttons
          submitBtn.hide();
          continueBtn.hide();


      });

      // Clicking on the spanish button:
      // Link takes user to Duolingo

  });

  function touchHandler(event) {
      var touches = event.changedTouches,
          first = touches[0],
          type = "";
      switch (event.type) {
          case "touchstart":
              type = "mousedown";
              break;
          case "touchmove":
              type = "mousemove";
              break;
          case "touchend":
              type = "mouseup";
              break;
          default:
              return;
      }


      // initMouseEvent(type, canBubble, cancelable, view, clickCount, 
      //                screenX, screenY, clientX, clientY, ctrlKey, 
      //                altKey, shiftKey, metaKey, button, relatedTarget);

      var simulatedEvent = document.createEvent("MouseEvent");
      simulatedEvent.initMouseEvent(type, true, true, window, 1,
          first.screenX, first.screenY,
          first.clientX, first.clientY, false,
          false, false, false, 0 /*left*/ , null);

      first.target.dispatchEvent(simulatedEvent);
      event.preventDefault();
  }


  document.getElementsByClassName('slikica')[0].addEventListener("touchstart", touchHandler, true);
  document.getElementsByClassName('slikica')[0].addEventListener("touchmove", touchHandler, true);
  document.getElementsByClassName('slikica')[0].addEventListener("touchend", touchHandler, true);
  document.getElementsByClassName('slikica')[0].addEventListener("touchcancel", touchHandler, true);

  $('.slikica').lightzoom({
      glassSize: 175,
      zoomPower: 2
  });

  $(document).mouseup(function(e) {
      var container = $(".slikica, .slikica2");

      // if the target of the click isn't the container nor a descendant of the container
      if (!container.is(e.target) && container.has(e.target).length === 0) {

          $("#glass").hide();
      }



  });